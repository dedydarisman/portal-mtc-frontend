import axios from 'axios'

export const state = () => ({
  hwApp: 0,
  swApp: 0,
  logApp: 0,
  adminReq: 0,
  dataAdmin: [],
  hwMember: 0,
  // swMember: 0,
  // logMember: 0,
})

export const actions = {
  gethwApp({ commit }) {
    axios
      .get('https://compnetmtc.com:5003/api/v1/portal/hardware_approval/')
      .then((response) => {
        commit('SET_HWAPP', response.data.length)
      })
  },
  getswApp({ commit }) {
    axios
      .get('https://compnetmtc.com:5003/api/v1/portal/software_approval/')
      .then((response) => {
        commit('SET_SWAPP', response.data.length)
      })
  },
  getlogApp({ commit }) {
    axios
      .get('https://compnetmtc.com:5003/api/v1/portal/log_approval/')
      .then((response) => {
        commit('SET_LOGAPP', response.data.length)
      })
  },
  adminReq({ commit }) {
    axios
      .get('https://compnetmtc.com:5003/api/v1/portal/req_admin/')
      .then((response) => {
        commit('SET_ADMINREQ', response.data.length)
      })
  },
  listAdmin({ commit }) {
    axios
      .get('https://compnetmtc.com:5003/api/v1/portal/admin/')
      .then((response) => {
        commit('SET_DATAADMIN', response.data)
      })
  },
  gethwMember({ commit }, { username }) {
    axios
      .post(
        'https://compnetmtc.com:5003/api/v1/portal/hardware_approval/member/',
        {
          username,
        }
      )
      .then((response) => {
        commit('SET_HWMEMBER', response.data.length)
      })
  },
  getswMember({ commit }, { username }) {
    axios
      .post(
        'https://compnetmtc.com:5003/api/v1/portal/software_approval/member/',
        {
          username,
        }
      )
      .then((response) => {
        commit('SET_SWMEMBER', response.data.length)
      })
  },
  getlogMember({ commit }, { username }) {
    axios
      .post('https://compnetmtc.com:5003/api/v1/portal/log_approval/member/', {
        username,
      })
      .then((response) => {
        commit('SET_LOGMEMBER', response.data.length)
      })
  },
}

export const mutations = {
  SET_HWAPP(state, hwApp) {
    state.hwApp = hwApp
  },
  SET_SWAPP(state, swApp) {
    state.swApp = swApp
  },
  SET_LOGAPP(state, logApp) {
    state.logApp = logApp
  },
  SET_ADMINREQ(state, adminReq) {
    state.adminReq = adminReq
  },
  SET_DATAADMIN(state, listAdmin) {
    state.listAdmin = listAdmin
  },
  SET_HWMEMBER(state, hwMember) {
    state.hwMember = hwMember
  },
  SET_SWMEMBER(state, swMember) {
    state.swMember = swMember
  },
  SET_LOGMEMBER(state, logMember) {
    state.logMember = logMember
  },
}
