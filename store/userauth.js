export const state = () => ({
  name: '',
  useremail: '',
  isLoggedIn: false,
  isAdmin: false,
  isLevel: '',
})

export const mutations = {
  login(state, userdata) {
    state.isLoggedIn = true
    state.isAdmin = userdata.isAdmin
    state.name = userdata.renameuser
    state.useremail = userdata.email
    state.isLevel = userdata.level
  },
  logout(state) {
    state.isLoggedIn = false
    state.isAdmin = false
    state.name = ''
    state.useremail = ''
    state.isLevel = ''
  },
}
