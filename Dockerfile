# Dockerfile
FROM node:11.13.0-alpine

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git

RUN apk update && apk add tzdata
ENV TZ="Asia/Jakarta"

# create destination directory
RUN mkdir -p /usr/src/nuxt-app
WORKDIR /usr/src/nuxt-app

# copy the app, note .dockerignore
COPY . /usr/src/nuxt-app/
RUN npm install
RUN npm run build

EXPOSE 5000

ENV NUXT_HOST=127.0.0.0
ENV NUXT_PORT=5000

CMD [ "npm", "start" ]