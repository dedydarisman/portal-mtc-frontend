import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader version "^2.1.1" ,
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
Vue.use(Vuetify)

export default (ctx) => {
  const vuetify = new Vuetify({
    theme: {
      light: true,
      dark: false,
      themes: {
        light: {
          primary: '#036b84',
          secondary: '#fff',
          accent: '#3ea2fb',
          error: '#dc3545',
          petrol: '#17a499',
          background: '#fff',
          black: '#000',
          white: '#fff',
          // ludes: 'background: linear-gradient(to left, #141e30, #243b55)',
        },
        dark: {
          primary: '#fff',
          secondary: '#036b84',
          accent: '#3ea2fb',
          error: '#dc3545',
          petrol: '#17a499',
          background: '#000',
          black: '#000',
          white: '#fff',
          // ludes: 'background: linear-gradient(to left, #141e30, #243b55)',
        },
      },
    },
    icons: {
      iconfont: 'fa',
    },
  })

  ctx.app.vuetify = vuetify
  ctx.$vuetify = vuetify.framework
}
// src/plugins/vuetify.js
